package com.sdiemert.springjwt.routes;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyResource {


    /**
     * A basic REST endpoint that we will protect using
     * JWT authentication scheme.
     */
    @RequestMapping(path = "/hello")
    public String hello(){
        return "hello world!";
    }

}
