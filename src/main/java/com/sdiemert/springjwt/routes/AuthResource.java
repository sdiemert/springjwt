package com.sdiemert.springjwt.routes;

import com.sdiemert.springjwt.model.*;
import com.sdiemert.springjwt.services.CustomUserDetailsService;
import com.sdiemert.springjwt.services.TokenRepository;
import com.sdiemert.springjwt.util.JWTUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value={"/auth"})
public class AuthResource {

    @Autowired
    private CustomUserDetailsService userService;

    @Autowired
    private TokenRepository tokenRepo;

    @Autowired
    private JWTUtil jwt;

    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> authenticate(@RequestBody AuthRequest req) throws Exception{

        try {

            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword()));

        }catch (BadCredentialsException e){

            throw new Exception("Invalid username or password!", e);

        }

        UserDetails userDetails = userService.loadUserByUsername(req.getUsername());

        String accessTokStr = jwt.generateAccessToken(userDetails);
        String refreshTokStr = jwt.generateRefreshToken(userDetails);

        // store the generated refresh token.
        Token refreshTok = new Token();
        refreshTok.token = refreshTokStr;
        refreshTok.username = userDetails.getUsername();
        tokenRepo.save(refreshTok);

        return ResponseEntity.ok(new AuthResponse(accessTokStr, refreshTokStr));

    }

    @RequestMapping(path = "/refresh", method = RequestMethod.POST)
    public ResponseEntity<?> refresh(@RequestBody RefreshRequest req) throws Exception{

        String username = null;
        String accessTok = null;
        Token storedRequestToken = null;

        try {

            // will fail if the token is expired.
            username = jwt.extractUsername(req.refreshToken);

        }catch(ExpiredJwtException ex){
            return new ResponseEntity<>("refresh token expired", HttpStatus.FORBIDDEN);
        }

        // load the matching token from the data store.
        if(username != null){

            // should be null if no token exists.
            storedRequestToken = tokenRepo.findByUsername(username);

            // ensure the tokens match and still valid.
            if(storedRequestToken.token.equals(req.refreshToken)){
                // now we can issue a new access token
                accessTok = jwt.generateAccessToken(userService.loadUserByUsername(username));
                return ResponseEntity.ok(new AuthResponse(accessTok, null));
            }
        }

        // otherwise failed to issue token
        return new ResponseEntity<>("invalid refresh token", HttpStatus.FORBIDDEN);
    }

    @RequestMapping(path = "/logout", method = RequestMethod.POST)
    public ResponseEntity<?> logout(){
        invalidateRefreshToken(SecurityContextHolder.getContext().getAuthentication().getName());
        return new ResponseEntity<>("logged out", HttpStatus.OK);
    }

    private void invalidateRefreshToken(String username){
        Token t = new Token();
        t.username = username;
        t.token = null;
        tokenRepo.save(t);
    }


}
