package com.sdiemert.springjwt.services;

import com.sdiemert.springjwt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Will be AUTOMATICALLY satisfied by Spring's dependency injection.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    User findUserByUsername(String username);

}
