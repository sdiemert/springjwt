package com.sdiemert.springjwt.services;

import com.sdiemert.springjwt.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    /**
     * Looks up a User based on the provided username.
     * @param s the username.
     * @return the User that is matched.
     * @throws UsernameNotFoundException if the username does not match in the user data store.
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User u = userRepo.findUserByUsername(s);

        // Translate the JPA Entity User into a Spring Security User object
        // which has additional default behaviours. Not sure if this is the best
        // choice for a real application.
        return new org.springframework.security.core.userdetails.User(
                u.getUsername(), u.getPassword(), new ArrayList<>());

    }
}
