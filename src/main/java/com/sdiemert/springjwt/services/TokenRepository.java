package com.sdiemert.springjwt.services;

import com.sdiemert.springjwt.model.Token;
import com.sdiemert.springjwt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Will be AUTOMATICALLY satisfied by Spring's dependency injection.
 */
public interface TokenRepository extends JpaRepository<Token, Integer> {

    Token findByUsername(String username);

}
