package com.sdiemert.springjwt.model;

public class AuthResponse {

    private final String access;
    private final String refresh;

    public AuthResponse(String accessToken, String refreshToken){
        this.access = accessToken;
        this.refresh = refreshToken;
    }

    public String getAccess() {
        return access;
    }

    public String getRefresh() {
        return refresh;
    }
}
