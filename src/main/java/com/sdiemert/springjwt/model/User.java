package com.sdiemert.springjwt.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    private String username;

    private String password;

    public String toString(){
        return "{ username : '" + username + "', password : '" + password + "' }";
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
