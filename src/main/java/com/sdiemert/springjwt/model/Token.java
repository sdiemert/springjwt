package com.sdiemert.springjwt.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Token {
    @Id
    public String username;
    public String token;
}
