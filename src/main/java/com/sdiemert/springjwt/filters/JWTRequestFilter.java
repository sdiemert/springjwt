package com.sdiemert.springjwt.filters;

import com.sdiemert.springjwt.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@Component
public class JWTRequestFilter extends OncePerRequestFilter {

    @Autowired
    private JWTUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {

        String authHeader = req.getHeader("Authorization");

        String username = null;
        String jwt = null;

        if(authHeader != null && authHeader.startsWith("Bearer")){

            /*
             * Extract the jwt from the authorization header.
             * The expected format is: "Bearer xxxx..." where xxxx... is the JSON Web Token.
             */
            jwt = authHeader.substring(7);

            /*
             * Extracting the username from the token checks the signature and expiration automatically.
             *
             * Throws an exception if the token is invalid.
             *
             * The exception stops the filter proceeding (next in the chain) which means the
             * Spring UsernamePasswordAuthenticationToken will not be set and will fail
             * authentication via the default Spring Security mechanism.
             */
            username = jwtUtil.extractUsername(jwt);

        }

        // username provided AND not already authenticated.
        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){

            /*
             * Create the default Spring Security authentication token based on the
             * information provided in the JWT token.
             */
            UsernamePasswordAuthenticationToken tempToken =
                new UsernamePasswordAuthenticationToken(username, null, new ArrayList<>());

            /*
             * Store the Spring Security token for validation later by the
             * default authentication mechanism.
             */
            SecurityContextHolder.getContext().setAuthentication(tempToken);

        }

        /*
         * Proceed with the Spring Security filters with or without the authentication token set.
         */
        chain.doFilter(req, res);

    }
}
