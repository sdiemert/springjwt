package com.sdiemert.springjwt.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Utility class for managing JSON Web Tokens
 */
@Service
public class JWTUtil {

    private String SECRET_KEY = "supersecret";
    private int ACCESS_TOKEN_EXPIRE_min = 15;
    private int REFRESH_TOKEN_EXPIRE_min = 60;

    public String generateAccessToken(UserDetails user){
        // empty, no claims yet.
        HashMap<String, Object> claims = new HashMap<>();
        return createToken(claims, user.getUsername(), ACCESS_TOKEN_EXPIRE_min);
    }

    public String generateRefreshToken(UserDetails user){
        // empty, no claims yet.
        HashMap<String, Object> claims = new HashMap<>();
        return createToken(claims, user.getUsername(), REFRESH_TOKEN_EXPIRE_min);
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public boolean tokenExpired(String jwt) {
        Date now = new Date(System.currentTimeMillis());
        return extractExpiration(jwt).before(now);
    }

    private String createToken(Map<String, Object> claims, String subject, int expire_min){

        Date now = new Date(System.currentTimeMillis());
        Date expiry = new Date(System.currentTimeMillis() + 1000 * 60 * expire_min);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(now)
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();

    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }
}
