# Spring JWT Authentication Demo

This repository contains a demo implementation of JSON Web Token (JWT) based authentication/authorization. 

The function is as follows: 

* User accounts already exist in a database. 

* A user makes POSTs their username and password to `/auth`. The server checks their credentials and issues two JWTs if successful: 1) an *access token*, and 2) a *refresh token*. The access token expires in 15 minutes. The refresh token expires in 60 minutes. 

* A user may GET `/hello` if they have a valid JWT access token (unexpired, cryptographically sound, etc.). The server checks the access token and returns "hello world!" if the JWT is valid.

* A user may POST to `/auth/refresh` with their refresh token to obtain a new access token (even if the access token is already expired). The server checks the refresh token and returns a new access token as long as the refresh token is valid.

* A user may POST to `/auth/logout` to invalidate the existing refresh token. 

This is meant to be accessed exclusively via web requests (i.e., not the browser). I used Postman to test the behaviour.

## Running

This demo application assumes that a MySQL database is available at `127.0.0.1:3306`. See `infra/db` for details on setting up this as a Docker container.

### IDE 
This code uses Spring Boot to run the web server. This can be run like any Spring Boot application from an IDE. 

### Command Line

To run this from command line only: 

1. Build the application using Maven: `$ mvn package`. 

2. Run the application's jar using java: `$ java -jar target/springjwt-0.0.1-SNAPSHOT.jar`.

### Docker
This may be run as a Docker container using the Dockerfile in the root project directory.

## Testing 

The following cURL commands can be used to test the application. Execute them from local host. 

### Authentication

Obtain the access and refresh JWTs using the following: 

```bash
curl -X POST \
    -H "Content-Type: application/json"\
    -H "Cache-Control: no-cache" \ 
    -d '{"username" : "foo", "password" : "foo"}' \
    "http://localhost:8080/auth"
```

Should return something like: 

```json
{
  "access" : "your.access.token",
  "refresh": "your.refresh.token"
}
```

### Access Secured Route

Access the secured route by placing the access JWT in an Authorization header per the JWT standard: 

```bash
curl -X GET -H "Content-Type: application/json" \
    -H "Authorization: Bearer your.access.token" \
    -H "Cache-Control: no-cache" "http://localhost:8080/hello"
```

This should return the text: `hello world!`. 

### Refresh Access Token

Submit the refresh token to obtain a new access token: 

```bash
curl -X POST -H "Content-Type: application/json" \
    -H "Cache-Control: no-cache" \
    -d '{ "refreshToken" : "your.refresh.token"}' \
    "http://localhost:8080/refresh"
```

This should return: 

```json
{
  "access": "new.access.token",
  "refresh": null
}
```

Notice that a new refresh token is not provided. Continue to use the previous refresh token. 

### Logout

Invalidate the existing refresh token using: 

```bash
curl -X POST \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer new.access.token" \
    -H "Cache-Control: no-cache" \
    "http://localhost:8080/auth/logout"
```

This should return `logout successful`.


