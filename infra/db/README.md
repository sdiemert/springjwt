# DB Infrastructure

Infrastructure for hosting a toy database for sample user details to support this Spring + JWT demo implementation.

## Building Docker Image

The Dockerfile in this directory creates a container running MySQL 5.7 and copies the `load.sql` script into the mysql startup folder. Anything in this folder is automatically executed by MySQL on boot.

Build the image using: `$ docker build -t springjwt .`.

Run `$ docker ps -a` to see built image listed.

## Run Using Docker

Run container using: `$ docker run -d -p 3306:3306 --name springjwt -e MYSQL_ROOT_PASSWORD=mypassword springjwt`.

* `-d` detaches the container from the present console.
* `-p 3306:3306` maps the container's mysql to port 3306 on this host.

Run `$ docker ps` to see running container.

## Connecting to MySQL Server

Connect to the MySQL server running on the springjwt container using: `$ mysql -u root -h 127.0.0.1 -P 3306 -p`.

Use the password listed above when prompted.


## Removing Container

If you run the container multiple times you will have to remove the container using `$ docker rm springjwt`.

## References: 

* [https://medium.com/better-programming/customize-your-mysql-database-in-docker-723ffd59d8fb](https://medium.com/better-programming/customize-your-mysql-database-in-docker-723ffd59d8fb)






