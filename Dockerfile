FROM openjdk:11
ARG JAR_FILE=target/*.jar
ENV HOST_ADDRESS=host.docker.internal
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]